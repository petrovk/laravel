
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import { required, email, maxLength } from 'vuelidate/lib/validators';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(Validate);
Vue.component('label-danger', require('./components/LabelDanger.vue'));
Vue.component('alert-success', require('./components/AlertSuccess.vue'));
Vue.component('rows-applications', require('./components/RowsApplications.vue'));
Vue.component('alert-error', require('./components/AlertError.vue'));

const app = new Vue({
    el: '#app',
    mixins: [Validate.validationMixin],
    data: {
        email: '',
        text: '',
        success: false,
        disabled: false,
        rows: null,
        error: null
    },
    computed: {
        count: function () {
            return this.text.length
        }
    },
    methods: {
        onSubmit: function () {
            this.$v.$touch();

            if (!this.$v.email.$error && !this.$v.text.$error) {

                this.disabled = true;

                this.$http.put('/', {
                    email: this.email,
                    text: this.text
                })
                .then( function(response) {

                    const json = response.json();

                    json.then( result =>  {

                        //console.log('done', result.data);

                        this.rows = result.data;

                        this.email = '';
                        this.text = '';
                        this.success = true;

                        setTimeout(() => {
                            this.success = false;
                            this.disabled = false;
                        }, 2000)
                    })

                }, function(response) {
                    // error callback
                    const status = response.status;
                    if (status === 500 || status === 0) {
                        this.error = true;
                    }

                    setTimeout(() => {
                        this.error = null;
                    }, 2000)

                    this.disabled = false;

                    console.log('error', response)
                });
            }

        },
    },
    validations: {
        email: {
            required,
            email
        },
        text: {
            maxLength: maxLength(5000)
        },

    }
});
