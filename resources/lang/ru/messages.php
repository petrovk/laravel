<?php

return [

    'panel_title' => 'Заявки',
    'panel_title_form' => 'Создание заявки',
    'count' => 'Количество символов :count / 5000',
    'email' => 'Неверный адрес электронной почты',
    'field_label_email' => 'Адрес e-mail',
    'field_label_text' => 'Текст заявки',
    'btn_submit_send' => 'Отправить',
    'email_required' => 'Необходимо заполнить',
    'application_success' => 'Заявка успешно добавлена.',
    'table_th_n' => '№ заявки',
    'table_th_user' => 'Пользователь',
    'table_th_date' => 'Дата создания',
    'error' => 'Сервис не отвечает. Попробуйте позже.',
];
