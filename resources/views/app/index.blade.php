@extends('base')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if ( isset($rows) && count($rows) )
                    <div class="panel panel-default" v-if="!rows">
                        <div class="panel-heading">@lang('messages.panel_title')</div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-md-2">@lang('messages.table_th_n')</th>
                                        <th class="col-md-8">@lang('messages.table_th_user')</th>
                                        <th class="col-md-4">@lang('messages.table_th_date')</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($rows as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif

                <rows-applications v-if="rows"
                                  :rows="rows"
                                  panel_title="@lang('messages.panel_title')"
                                  thn="@lang('messages.table_th_n')"
                                  thuser="@lang('messages.table_th_user')"
                                  thdate="@lang('messages.table_th_date')">

                </rows-applications>

                <div class="panel panel-default">
                    <div class="panel-heading">@lang('messages.panel_title_form')</div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'home.store', 'method' => 'put', 'v-on:submit.prevent' => 'onSubmit']) !!}
                        <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}"
                             v-bind:class="{ 'has-error': $v.email.$error }">
                            {!! Form::label('email', trans('messages.field_label_email'), ['class' => 'required control-label']) !!}
                            {!! Form::email('email', isset($request) ? $request['email'] : null, ['class' => 'form-control', 'v-model.trim' => "email", '@input' => '$v.email.$touch()' ]) !!}
                            <label-danger v-if="!$v.email.email" lang="@lang('messages.email')"></label-danger>
                            @if ($errors->first('email')) <span
                                    class="label label-danger">{{ $errors->first('email') }}</span> @endif
                        </div>
                        <div class="form-group {{ $errors->first('text') ? 'has-error' : '' }}"
                             v-bind:class="{ 'has-error': $v.text.$error }">
                            {!! Form::label('text', trans('messages.field_label_text'), ['class' => 'control-label']) !!}
                            {!! Form::textarea('text', isset($request) ? $request['text'] : null, ['class' => 'form-control', 'maxlength' => 5000, 'rows' => 3, 'v-model.trim' => "text", '@input' => '$v.text.$touch()']) !!}
                            @if ($errors->first('text'))
                                <span class="label label-danger">{{ $errors->first('text') }}</span>
                            @else
                                <span class="label"
                                      v-bind:class="{ 'label-danger': $v.text.$error, 'label-success': !$v.text.$error}">@lang('messages.count', ['count' => '@{{ count }}' ])</span>
                            @endif
                        </div>
                        <div class="form-group clearfix" v-if="!$v.text.$error">
                            {!! Form::submit(trans('messages.btn_submit_send'), ['class' => 'btn btn-primary pull-right', ':class' => "{ 'disabled': disabled }", ':disabled' => 'disabled']) !!}
                        </div>
                        {!! Form::close() !!}

                    </div>

                </div>
                <transition name="fade">
                    <alert-success v-if="success" message="@lang('messages.application_success')"></alert-success>
                    <alert-error v-if="error" message="@lang('messages.error')"></alert-error>
                </transition>
            </div>
        </div>
    </div>

@stop