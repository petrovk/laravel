<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'applications';

    protected $fillable = ['email', 'text'];

    public function getCreatedAtAttribute($attr)
    {
        $Carbon = new Carbon($attr);
        $createdAt = $Carbon->format('d.m.Y');
        return $createdAt;
    }

    public function scopeEmail($query, $email)
    {
        return $query->where('email', '=', $email);
    }

    public function scopeGetAll($query)
    {
        return $query->where('id', '!=', 0)->orderBy('id','desc');
    }

}
