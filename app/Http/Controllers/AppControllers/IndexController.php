<?php

namespace App\Http\Controllers\AppControllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Application;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(Application $applicationModel)
    {
        $rows = $applicationModel->getAll()->get();

        return view('app.index', ['rows' => $rows]);
    }

    /**
     * Сохранить заявку.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Application $applicationModel, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'text' => 'string|max:5000',
        ]);

        if ($validator->fails()) {
            // Переданные данные не прошли проверку
            $messages = $validator->messages();

            if ($request->ajax()) {
                return ['errors' => $messages];
            }

            return redirect('/')->withErrors($validator)->withInput($request->all());
        }
        else {
            $applicationModel->email($request->input('email'))->delete();
            $rows = $applicationModel->create($request->all())->getAll()->get();

            if ($request->ajax()) {
                return ['data' => $rows];
            }

            return redirect('/');
        }

    }

}